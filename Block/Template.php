<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUndefinedClassInspection */
namespace Smartwave\Porto\Block;
use Magento\Backend\Block\Template\Context;
use \Magento\Customer\Model\Session;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Smartwave\Porto\Model\Config\Backend\Image\Logo;
class Template extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Registry
     */
    public $_coreRegistry;
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * Template constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Session $customerSession,
        array $data = []
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }
    /**
     * @param $config_path
     * @param null $storeCode
     * @return mixed
     */
    public function getConfig($config_path, $storeCode = null)
    {
        return $this->_scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE,
            $storeCode
        );
    }
    /**
     * @return string
     */
    public function getFooterLogoSrc()
    {
        $folderName = Logo::UPLOAD_DIR;
        $storeLogoPath = $this->_scopeConfig->getValue(
            'porto_settings/footer/footer_logo_src',
            ScopeInterface::SCOPE_STORE
        );
        $path = $folderName . '/' . $storeLogoPath;
        return $this->_urlBuilder
                ->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $path;
    }
    /**
     * @return bool
     */
    public function isHomePage()
    {
        $currentUrl = $this->getUrl('', ['_current' => true]);
        $urlRewrite = $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        return $currentUrl == $urlRewrite;
    }
    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}
